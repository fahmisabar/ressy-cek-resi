import React from 'react';

import { StyleSheet, Text, View, TextInput } from 'react-native';
import { AntDesign } from '@expo/vector-icons'; 

export const SearchBox = (props) => {
    return(
        <View style={styles.formGroup}>
            <TextInput placeholderTextColor="#69707B"  style={styles.input} value={props.value} onChangeText={props.actions} placeholder="Masukan no resi kamu"></TextInput>
            <AntDesign style={styles.searchIcon} name="search1" size={34} color="#fff" />
        </View>
    )
}


const styles = StyleSheet.create({
    searchIcon: {
        position: 'absolute',
        bottom: 20,
        right: 25,
        opacity: 0.3
    },
    formGroup: {
        position: 'relative',
    },
    input: {
        backgroundColor: '#393E46',
        color: '#fff',
        fontSize: 18,
        fontWeight: '400',
        paddingHorizontal: 35,
        paddingVertical: 25,
        borderRadius: 5,
    }

})