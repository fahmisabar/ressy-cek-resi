import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';


// import page 
import Finder from './screen/apps/Finder';

export default function App() {
  return (
    <Finder></Finder>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
