import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Pressable, Image, ScrollView } from 'react-native';

import { SearchBox } from '../../components/Form';
import { Feather } from '@expo/vector-icons'; 
import { AntDesign } from '@expo/vector-icons'; 

import * as axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';


import Loading from '../../assets/ld.gif';
import Delivered from '../../assets/delivered.png';
import Ship from '../../assets/ship.png';

function Finder() {
    const [awb, setAwb] = useState('');
    const [courier, setCourier] = useState('')
    const [jne, setJne] = useState(false)
    const [jnt, setJnt] = useState(false)
    const [sicepat, setSicepat] = useState(false)
    const [anter, setAnter] = useState(false)
    const [tiki, setTiki] = useState(false)
    const [filled, setFilled] = useState();
    const [isloading, setIsloading] = useState(false);
    const [dataSumary, setDataSummary] = useState([]);
    const [dataHistory, setDataHistory] = useState([]);
    const [dataDetail, setDataDetail] = useState(false);
    const [errmsg, setErrmsg] = useState('');
    const [isError, setIsError] = useState(false);
    const [hasHstory, setHasHostory] = useState(false);


    const [service, setService] = useState();
    const [status, setStatus] = useState();
    const [date, setDate] = useState();
    const [weight, setWeight] = useState();
    const [receiver, setReceiver] = useState();
    const [shipper, setShipper] = useState();

    
    function selectJne() {
        setCourier('jne');
        setJne(true);
        setJnt(false);
        setSicepat(false);
        setAnter(false);
        setTiki(false)
        console.log(awb)
    }

    function selectJnt() {
        setCourier('jnt');
        setJne(false);
        setJnt(true);
        setSicepat(false);
        setAnter(false);
        setTiki(false)
    }

    function selectSicepat() {
        setCourier('sicepat');
        setJne(false);
        setJnt(false);
        setSicepat(true);
        setAnter(false);
        setTiki(false)
    }

    function selectAnteraja() {
        setCourier('anteraja');
        setJne(false);
        setJnt(false);
        setSicepat(false);
        setAnter(true);
        setTiki(false)
    }

    function selectTiki() {
        setCourier('tiki');
        setJne(false);
        setJnt(false);
        setSicepat(false);
        setAnter(false);
        setTiki(true)
    }

    const submit = async () => {
        if (awb == '' && courier == '') {
            setIsError(true)
            setErrmsg('masukan nomer resi dan pilih kurir')
        } else if (awb == '') {
            setIsError(true)
            setErrmsg('masukan nomer resei')
        } else if (courier == '') {
            setIsError(true)
            setErrmsg('Pilih kurir')
        } else {
            setIsError(false)
            setIsloading(true);
            await axios
            .get('https://api.binderbyte.com/v1/track', {
                params: {
                    api_key: '6a873e417d565de74ae87a30e399dbe1230a34d902d0ccbf3e725340d27f2950',
                    courier: courier,
                    awb: awb,
                }
            })
            .then(res=> {
                console.log(res.data.data);
                setDataHistory(res.data.data.history);
                setService(res.data.data.summary.service);
                setStatus(res.data.data.summary.status);
                setDate(res.data.data.summary.date);
                setWeight(res.data.data.summary.weight);
                setReceiver(res.data.data.detail.receiver);
                setShipper(res.data.data.detail.shipper);
                setIsloading(false);
                setDataDetail(true)
            })
            .catch(e => {
                console.log(e.response.data.message)
                setIsloading(false);
                setIsError(true)
                setErrmsg('Data ga ada ditemukan bor/sis, pastikan Resi yang kamu masukan betul yach.')
            })
        }
    }

    if (isloading) {
        return(
            <View style={styles.containerLoading}>
                <Image style={{width: 60, height: 60,}} source={Loading}></Image>
            </View>
        )
    }

    if (dataDetail) {
        return(
            <View style={styles.containerDetail}>
                <Pressable onPress={() => setDataDetail(false)}><AntDesign name="back" size={24} color="#FFD369" /></Pressable>
                <ScrollView style={{flex: 1, marginTop: 30,}} showsVerticalScrollIndicator={false}>
                    <View style={styles.statusIcon}>
                        <Image style={{width: 180, height: 180}} source={status == 'DELIVERED' ? Delivered : Ship} ></Image>
                    </View>
                    
                    
                    <View style={styles.inlineBox}>
                        <View style={styles.labelGroup}>
                            <Text style={styles.label}>No Resi</Text>
                            <Text style={styles.dataInfo}>{awb}</Text>
                        </View>
                        <View style={styles.labelGroup}>
                            <Text style={styles.label}>Jasa Pengiriman</Text>
                            <Text style={styles.dataInfo}>{courier}</Text>
                        </View>
                    </View>

                    <View style={styles.inlineBox}>
                        <View style={styles.labelGroup}>
                            <Text style={styles.label}>Penerima</Text>
                            <Text style={styles.dataInfo}>{receiver}</Text>
                        </View>

                        <View style={styles.labelGroup}>
                            <Text style={styles.label}>Pengirim</Text>
                            <Text style={styles.dataInfo}>{shipper}</Text>
                        </View>
                    </View>

                    <View style={styles.inlineBox}>
                        <View style={styles.labelGroup}>
                            <Text style={styles.label}>Status</Text>
                            <Text style={styles.dataInfo}>{status}</Text>
                        </View>
                        <View style={styles.labelGroup}>
                            <Text style={styles.label}>Date</Text>
                            <Text style={styles.dataInfo}>{date}</Text>
                        </View>
                    </View>

                    <View style={styles.inlineBox}>
                        <View style={styles.labelGroup}>
                            <Text style={styles.label}>Wight</Text>
                            <Text style={styles.dataInfo}>{weight} Gr</Text>
                        </View>
                    </View>
                    
                    <View style={{marginTop: 30,}}></View>
                    { dataHistory.map((item, index) => {
                        return(
                            <View style={styles.hitoryContainer} key={index}>
                                <View style={styles.hPin}>
                                    <View style={styles.pin}></View>
                                    <View style={styles.batang}></View>
                                </View>
                                <View style={styles.hitory}>
                                    <Text style={styles.historyDate}>{item.date}</Text>
                                    <Text style={styles.historyDesc}>{item.desc}</Text>
                                    <Text style={styles.historyLoc}>{item.location}</Text>
                                </View>
                            </View>
                        )
                    })}
                </ScrollView>

                
            </View>
        )
    }

    return ( 
        <View style={styles.container}>
            <View style={styles.containerBox}>
                <Feather style={{marginBottom: 20, marginTop: 40,}} name="package" size={64} color="#FFD369" />
                <Text style={styles.greeting}>Hallo! Lacak pengirimanmu disini.</Text>
                
                { isError ? 
                <View style={styles.ErrorBox}>
                    <AntDesign name="exclamationcircleo" size={32} color="#513252" />
                    <Text style={styles.errText}>{errmsg}</Text>
                </View>
                :
                null
                }
                
                <SearchBox value={awb} actions={(value) => setAwb(value)}></SearchBox>
                <View style={{display: 'flex', flexDirection: 'row', marginTop: 20, flexWrap: 'wrap',}}>
                    <Pressable onPress={selectJne} style={styles.boxSelect}>
                        <Text style={ jne ? styles.SelectBoxActive : styles.SelectBox}>JNE</Text>
                    </Pressable>
                    <Pressable onPress={selectJnt} style={styles.boxSelect}>
                        <Text style={ jnt ? styles.SelectBoxActive : styles.SelectBox}>JNT</Text>
                    </Pressable>
                    <Pressable onPress={selectSicepat} style={styles.boxSelect}>
                        <Text style={ sicepat ? styles.SelectBoxActive : styles.SelectBox}>SICEPAT</Text>
                    </Pressable>
                    <Pressable onPress={selectAnteraja} style={styles.boxSelect}>
                        <Text style={ anter ? styles.SelectBoxActive : styles.SelectBox}>ANTER AJA</Text>
                    </Pressable>
                    <Pressable onPress={selectTiki} style={styles.boxSelect}>
                        <Text style={ tiki ? styles.SelectBoxActive : styles.SelectBox}>TIKI</Text>
                    </Pressable>
                </View>

                <Pressable style={{marginTop: 20,}} onPress={() => submit()}>
                    <Text style={styles.submitButton}>Cek Pengiriman</Text>
                </Pressable>

                <Text style={styles.footers}>Beeyond Creative Dev - v.0.1</Text>
            </View>
        </View>
     );
}

export default Finder;

const styles = StyleSheet.create({
    errText: {
        marginLeft: 15,
        fontSize:16,
        color: '#513252',
    },
    ErrorBox: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFC4C4',
        padding: 20,
        borderRadius: 5,
        marginBottom:10,
    },
    footers: {
        color: '#fff',
        textAlign: 'center',
        marginTop: 30,
        opacity: 0.2
    },
    statusIcon: {
        marginBottom: 60,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    hPin: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        marginRight: 20,
        position: 'relative',
        overflow: 'hidden',
    },
    batang: {
        width: 3,
        flex: 1,
        zIndex: 0,
        height: 500,
        position: 'absolute',
        backgroundColor: '#393E46',
    },
    pin: {
        width: 10,
        height: 10,
        zIndex: 2,
        backgroundColor: '#FFD369',
        borderColor: '#FFD369',
        borderRadius: 50,
    },
    hitoryContainer: {
        display: 'flex',
        flexDirection: 'row',
    },
    hitory: {
        backgroundColor: '#393E46',
        padding: 20,
        borderRadius: 10,
        marginBottom: 20,
        flex: 1,
    },
    historyDate: {
        color: '#eee',
        fontSize: 14,
        fontWeight: '800',
        opacity: 0.5,
        marginBottom: 15,
    },
    historyLoc: {
        color: '#eee',
        fontSize: 14,
        fontWeight: '400',
        opacity: 0.8,
        marginBottom: 5,
    },
    historyDesc: {
        color: '#fff',
        fontSize: 14,
        lineHeight: 24,
        marginBottom: 10
    },
    inlineBox: {
        display: 'flex',
        flexDirection: 'row',
        marginHorizontal: -10,
    },
    labelGroup: {
        marginBottom: 20,
        marginHorizontal: 10,
        flex: 1,
    },
    label: {
        color: '#eee',
        fontSize: 14,
        fontWeight: '400',
        opacity: 0.8,
        marginBottom: 5,
    },
    dataInfo: {
        color: '#FFD369',
        fontSize: 18,
        fontWeight: '700',
    },
    boxSelect: {
        marginBottom: 15,
    },
    SelectBox: {
        color: '#FFD369',
        borderColor: '#FFD369',
        borderWidth: 1,
        paddingVertical: 15,
        paddingHorizontal: 30,
        fontWeight: '700',
        marginRight: 10,
        fontSize: 14,
        borderRadius: 50,
    },

    SelectBoxActive: {
        color: '#000',
        backgroundColor: '#FFD369',
        borderColor: '#FFD369',
        borderWidth: 1,
        paddingVertical: 15,
        paddingHorizontal: 30,
        fontWeight: '700',
        marginRight: 10,
        fontSize: 14,
        borderRadius: 50,
    },
    submitButton: {
        backgroundColor: '#FFD369',
        textAlign: 'center',
        paddingHorizontal: 25,
        paddingVertical: 20,
        borderRadius: 5,
        marginTop: 20,
        fontWeight: 'bold',
        fontSize: 20
    },
    greeting: {
        fontSize: 32,
        color: '#fff',
        width: 350,
        fontWeight: '300',
        marginBottom: 25,
    },
    container: {
      flex: 1,
      backgroundColor: '#222831',
      justifyContent: 'flex-end',
      paddingBottom: 10,
    },
    containerLoading: {
        flex: 1,
        backgroundColor: '#222831',
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerDetail: {
        flex: 1,
        backgroundColor: '#222831',
        justifyContent: 'flex-start',
        paddingHorizontal: 25,
        paddingTop: 60,
    },
    containerBox: {
        padding: 20,
    },
  });
  